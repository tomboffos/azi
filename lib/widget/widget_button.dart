import 'package:azi/common/app_style.dart';
import 'package:flutter/material.dart';

import '../common/app_colors.dart';

class Button extends StatefulWidget {
  final String text;
  double width;
  double heigh;
  dynamic onPressed;
  Color? color;
  Button(
      {required this.text,
      required this.heigh,
      required this.width,
      required this.onPressed,
      this.color});
  @override
  _Button createState() => _Button();
}

class _Button extends State<Button> {
  @override
  Widget build(BuildContext context) {
    {
      return Container(
        margin: const EdgeInsets.symmetric(vertical: 15),
        width: widget.width,
        height: widget.heigh,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: widget.color ?? AppColors.black,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
          ),
          onPressed: widget.onPressed,
          child: Text(
            widget.text,
            style: AppTextStyles.menu,
            textAlign: TextAlign.center,
          ),
        ),
      );
    }
  }
}
