import 'package:azi/data/model/game_member.dart';
import 'package:azi/widget/widget_button.dart';
import 'package:flutter/material.dart';

class UserGameWidget extends StatelessWidget {
  final List<GameMember> gameMembers;
  final GameMember gameMember;
  const UserGameWidget(
      {Key? key, required this.gameMembers, required this.gameMember})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Button(
        text: gameMembers.length > 0
            ? '${gameMember.ready ? 'Готово \n' : ''}${gameMember.user.name}'
            : 'Ожидаем игрока',
        heigh: 50,
        width: 150,
        onPressed: () {});
  }
}
