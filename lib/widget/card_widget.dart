import 'package:flutter/material.dart';
import 'package:playing_cards/playing_cards.dart';

class CardWidget extends StatelessWidget {
  final PlayingCard? card;
  const CardWidget({Key? key, this.card}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ShapeBorder shape = RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
        side: BorderSide(color: Colors.black, width: 1));
    return SizedBox(
      width: 80,
      child: PlayingCardView(
          card: card ?? PlayingCard(Suit.hearts, CardValue.ace),
          showBack: card == null,
          elevation: 3.0,
          shape: shape),
    );
  }
}
