import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:sizer/sizer.dart';

class PageEntry extends StatefulWidget {
  final bool isVisible;
  final String hinttext;
  final dynamic texttype;
  final dynamic sufficicon;
  final bool obcure;
  final TextEditingController? controller;
  final MaskTextInputFormatter? formatter;
  final onChanged;
  final String? errorText;
  PageEntry({
    this.isVisible = true,
    required this.hinttext,
    required this.texttype,
    required this.obcure,
    this.sufficicon,
    this.controller,
    this.formatter,
    this.onChanged,
    this.errorText,
  });
  @override
  _PageEntryState createState() => _PageEntryState();
}

class _PageEntryState extends State<PageEntry> {
  @override
  Widget build(BuildContext context) {
    {
      return Container(
        margin: EdgeInsets.all(10),
        width: 75.w,
        child: TextField(
          controller: widget.controller,
          obscureText: widget.obcure,
          keyboardType: widget.texttype,
          inputFormatters: widget.formatter != null ? [widget.formatter!] : [],
          autofocus: false,
          onChanged: widget.onChanged,
          style: TextStyle(fontSize: 15.0, color: Colors.black),
          decoration: InputDecoration(
            errorText: widget.errorText,
            suffixIcon: widget.sufficicon,
            border: InputBorder.none,
            hintText: widget.hinttext,
            filled: true,
            fillColor: Colors.white,
            contentPadding:
                const EdgeInsets.only(left: 14.0, bottom: 6.0, top: 8.0),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.red),
              borderRadius: BorderRadius.circular(10.0),
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.grey),
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
        ),
      );
    }
  }
}
