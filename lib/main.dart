import 'package:azi/firebase_options.dart';
import 'package:azi/injection.dart';
import 'package:azi/internal/application.dart';
import 'package:azi/router.dart';
import 'package:azi/services/firebase_service.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  await FirebaseService().getToken();
  configureDependencies();
  runApp(MyApp(router: AppRouter()));
}
