import 'package:azi/common/app_colors.dart';
import 'package:azi/common/app_strings.dart';
import 'package:azi/common/app_style.dart';
import 'package:azi/flows/room/cubit/room_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';

class RoomsPage extends StatefulWidget {
  final int roomTypeId;
  RoomsPage({Key? key, required this.roomTypeId}) : super(key: key);

  @override
  _RoomsPageState createState() => _RoomsPageState();
}

class _RoomsPageState extends State<RoomsPage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    BlocProvider.of<RoomCubit>(context).fetchBids(widget.roomTypeId);
  }

  @override
  Widget build(BuildContext context) {
    final cubit = BlocProvider.of<RoomCubit>(context);
    return Scaffold(
      backgroundColor: AppColors.green2,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Center(
        child: Container(
          width: MediaQuery.of(context).size.width * 0.2.w,
          child: BlocBuilder<RoomCubit, RoomState>(
            builder: (context, state) {
              return ListView.builder(
                shrinkWrap: false,
                itemCount: cubit.bids!.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () => Navigator.pushNamed((context), '/filter-rooms',
                        arguments: {
                          'bid': cubit.bids![index],
                          'roomTypeId': widget.roomTypeId
                        }),
                    child: Container(
                      height: MediaQuery.of(context).size.height / 2.h,
                      decoration: BoxDecoration(
                        color: AppColors.greyccc,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      margin: EdgeInsets.all(5),
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              alignment: Alignment.center,
                              child: Text(
                                'Комнаты на ${cubit.bids![index]} ₸',
                                style: AppTextStyles.menu,
                              ),
                            ),
                            Icon(
                              Icons.arrow_forward_ios,
                              color: Colors.white,
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              );
            },
          ),
        ),
      ),
    );
  }
}
