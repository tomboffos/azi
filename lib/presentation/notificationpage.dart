import 'package:azi/common/app_colors.dart';
import 'package:azi/common/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class NotificationPage extends StatefulWidget {
  NotificationPage({Key? key}) : super(key: key);

  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.green2,
      appBar: AppBar(
        backgroundColor: AppColors.green2,
        title: Text(AppString.notification_page),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Center(
            child: ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return Container(
              margin: EdgeInsets.only(top: 5, right: 15, bottom: 10, left: 15),
              height: MediaQuery.of(context).size.height / 1.5.h,
              decoration: BoxDecoration(
                  border: Border.all(color: AppColors.black, width: 4),
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15)),
              child: Align(
                alignment: Alignment.center,
                child: Container(
                  child: Text(
                    AppString.notification_message,
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                        fontSize: 17),
                  ),
                ),
              ),
            );
          },
          itemCount: 20,
        )),
      ),
    );
  }
}
