import 'package:azi/common/app_colors.dart';
import 'package:azi/flows/friend/cubit/friend_cubit.dart';
import 'package:azi/widget/widget_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';

class FriendsDrawer extends StatefulWidget {
  const FriendsDrawer({Key? key}) : super(key: key);

  @override
  State<FriendsDrawer> createState() => _FriendsDrawerState();
}

class _FriendsDrawerState extends State<FriendsDrawer> {
  @override
  Widget build(BuildContext context) {
    final cubit = BlocProvider.of<FriendCubit>(context);
    return Drawer(
      backgroundColor: AppColors.green2,
      child: Container(
        margin: EdgeInsets.only(top: 7.5.h),
        child: DefaultTabController(
          length: 3,
          child: Column(
            children: [
              TabBar(indicatorColor: Colors.white, tabs: [
                Tab(
                  child: Text('Друзья'),
                ),
                Tab(
                  child: Text('Запросы'),
                ),
                Tab(
                  child: Text('Заявки'),
                ),
              ]),
              BlocBuilder<FriendCubit, FriendState>(
                builder: (context, state) {
                  return Expanded(
                    child: TabBarView(children: [
                      Column(
                        children: [
                          PageEntry(
                            hinttext: 'Введите кого хотите найти',
                            texttype: TextInputType.name,
                            obcure: false,
                            onChanged: (value) {
                              BlocProvider.of<FriendCubit>(context)
                                  .getFriends(search: value);
                            },
                          ),
                          ListView.builder(
                            padding: EdgeInsets.all(0.0),
                            itemBuilder: (context, index) => ListTile(
                              title: Text(
                                cubit.friends[index].name ?? '',
                                style:
                                    AppColors.styledText(16.0, FontWeight.bold),
                              ),
                              trailing: IconButton(
                                  onPressed: () {},
                                  icon: const Icon(
                                    Icons.close,
                                    color: AppColors.red,
                                  )),
                            ),
                            itemCount: cubit.friends.length,
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          PageEntry(
                            hinttext: 'Введите кого хотите найти',
                            texttype: TextInputType.name,
                            obcure: false,
                            onChanged: (value) {
                              cubit.getRequests(search: value);
                            },
                          ),
                          ListView.builder(
                            padding: EdgeInsets.all(0.0),
                            itemBuilder: (context, index) => ListTile(
                              title: Text(
                                cubit.requests[index].name ?? '',
                                style:
                                    AppColors.styledText(16.0, FontWeight.bold),
                              ),
                              trailing: IconButton(
                                  onPressed: () {},
                                  icon: const Icon(
                                    Icons.close,
                                    color: AppColors.red,
                                  )),
                            ),
                            itemCount: cubit.requests.length,
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          PageEntry(
                            hinttext: 'Введите кого хотите найти',
                            texttype: TextInputType.name,
                            obcure: false,
                            onChanged: (value) {
                              cubit.getOffers(search: value);
                            },
                          ),
                          ListView.builder(
                            padding: EdgeInsets.all(0.0),
                            itemBuilder: (context, index) => ListTile(
                              title: Text(
                                cubit.offers[index].name ?? '',
                                style:
                                    AppColors.styledText(16.0, FontWeight.bold),
                              ),
                              trailing: IconButton(
                                  onPressed: () {},
                                  icon: const Icon(
                                    Icons.close,
                                    color: AppColors.red,
                                  )),
                            ),
                            itemCount: cubit.offers.length,
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                          ),
                        ],
                      ),
                    ]),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
