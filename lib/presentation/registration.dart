import 'package:azi/common/app_colors.dart';
import 'package:azi/common/app_strings.dart';
import 'package:azi/widget/widget_button.dart';
import 'package:azi/widget/widget_textfield.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:azi/widget/widget_textfield.dart';

class RegistrationPage extends StatefulWidget {
  RegistrationPage({Key? key}) : super(key: key);

  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.green2,
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Image.asset(
                  'assets/images/cards.png',
                  width: 60.w,
                  height: 40.h,
                ),
              ),
              PageEntry(
                obcure: false,
                hinttext: AppString.email,
                texttype: TextInputType.emailAddress,
              ),
              PageEntry(
                obcure: false,
                hinttext: AppString.login1,
                texttype: TextInputType.text,
              ),
              PageEntry(
                obcure: true,
                hinttext: AppString.password,
                texttype: TextInputType.emailAddress,
              ),
              Button(
                text: AppString.register,
                heigh: MediaQuery.of(context).size.height * 0.008.h,
                width: MediaQuery.of(context).size.width * 0.19.w,
                onPressed: () => Navigator.pushNamedAndRemoveUntil(
                    context, '/homepage', (route) => false),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
