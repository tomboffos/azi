import 'package:azi/common/app_colors.dart';
import 'package:azi/common/app_strings.dart';
import 'package:azi/common/app_style.dart';
import 'package:azi/flows/game/cubit/game_cubit.dart';
import 'package:azi/flows/room/cubit/room_cubit.dart';
import 'package:azi/flows/user/cubit/user_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';

class FilterRooms extends StatefulWidget {
  final int bid;
  final int roomTypeId;
  FilterRooms({Key? key, required this.bid, required this.roomTypeId})
      : super(key: key);

  @override
  _FilterRoomsState createState() => _FilterRoomsState();
}

class _FilterRoomsState extends State<FilterRooms> {
  @override
  void initState() {
    // TODO: implement initState
    BlocProvider.of<RoomCubit>(context)
        .fetchRoomsByBids(widget.bid, widget.roomTypeId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final cubit = BlocProvider.of<RoomCubit>(context);
    return Scaffold(
      backgroundColor: AppColors.green2,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Center(
        child: Container(
          width: MediaQuery.of(context).size.width * 0.2.w,
          child: BlocBuilder<UserCubit, UserState>(
            builder: (context, userState) {
              if (userState is! UserAuthorized) {
                return Center(
                  child: CircularProgressIndicator(
                    color: AppColors.black,
                  ),
                );
              }
              return BlocBuilder<RoomCubit, RoomState>(
                builder: (context, state) {
                  if (state is RoomLoading)
                    return Center(
                      child: CircularProgressIndicator(
                        color: AppColors.black,
                      ),
                    );
                  return ListView.builder(
                    shrinkWrap: false,
                    itemCount: cubit.rooms!.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () async {
                          BlocProvider.of<GameCubit>(context).enterRoom(
                              context, cubit.rooms![index], userState.user);
                        },
                        child: Container(
                          height: MediaQuery.of(context).size.height / 2.h,
                          decoration: BoxDecoration(
                            color: AppColors.greyccc,
                            borderRadius: BorderRadius.circular(20),
                          ),
                          margin: EdgeInsets.all(5),
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    '${cubit.rooms![index].currentUsers}/${cubit.rooms![index].users} Игрока Сумма : ${cubit.rooms![index].bid} ₸',
                                    style: AppTextStyles.menu,
                                  ),
                                ),
                                Icon(
                                  Icons.arrow_forward_ios,
                                  color: Colors.white,
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  );
                },
              );
            },
          ),
        ),
      ),
    );
  }
}
