import 'package:azi/common/app_colors.dart';
import 'package:azi/data/model/game.dart';
import 'package:azi/data/model/game_member.dart';
import 'package:azi/flows/game/cubit/game_cubit.dart';
import 'package:azi/flows/user/cubit/user_cubit.dart';
import 'package:azi/widget/card_widget.dart';
import 'package:azi/widget/widget_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:playing_cards/playing_cards.dart';
import 'package:sizer/sizer.dart';
import 'package:spring/spring.dart';

class Room extends StatefulWidget {
  final Game game;
  Room({Key? key, required this.game}) : super(key: key);

  @override
  State<Room> createState() => _RoomState();
}

class _RoomState extends State<Room> {
  List<Widget> cards = [];
  ShapeBorder shape = RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(8),
      side: BorderSide(color: Colors.black, width: 1));
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GameCubit, GameState>(
      builder: (context, gameState) {
        if (gameState is GameInitial) {
          return SizedBox.shrink();
        }

        return BlocBuilder<UserCubit, UserState>(
          builder: (context, state) {
            if (state is! UserAuthorized) return SizedBox.shrink();
            GameMember memberUser = gameState.game!.members
                .where((element) => element.user.id == state.user.id)
                .toList()
                .cast<GameMember>()
                .first;
            List<GameMember> gameMembers = gameState.game!.members
                .expand(
                    (member) => [if (member.user.id != state.user.id) member])
                .toList()
                .cast<GameMember>();

            return Scaffold(
                appBar: AppBar(
                  backgroundColor: AppColors.green2,
                  elevation: 0.0,
                  title: Text('Игра ${gameState.game!.bid} тг'),
                  actions: [
                    Container(
                        child: Text(
                      'Ваш баланс ${gameState.game!.room.roomTypeId == 1 ? memberUser.user.fakeBalance : memberUser.user.balance} тг ',
                      style: TextStyle(fontSize: 10.sp),
                    ))
                  ],
                ),
                backgroundColor: AppColors.green2,
                body: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      gameState is EnteredToGame
                          ? Button(
                              text: gameMembers.length > 0
                                  ? '${gameMembers[0].ready ? 'Готово \n' : ''}${gameMembers[0].user.name}'
                                  : 'Ожидаем игрока',
                              heigh: 50,
                              width: 150,
                              onPressed: () {})
                          : Spring.slide(
                              slideType: SlideType.slide_in_top,
                              child: Column(
                                children: [
                                  Container(
                                      padding: EdgeInsets.only(top: 10.sp),
                                      width: 70.w,
                                      child: FlatCardFan(
                                          children: gameMembers[0]
                                              .cards!
                                              .map((e) => CardWidget(
                                                    card: e.getPlayingCard(),
                                                  ))
                                              .toList())),
                                  Text(gameMembers[0].points.toString())
                                ],
                              )),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            if (gameState is EnteredToGame)
                              if (gameState.game!.room.users > 2)
                                Button(
                                    text: gameMembers.length > 1
                                        ? '${gameMembers[1].ready ? 'Готово \n' : ''}${gameMembers[1].user.name}'
                                        : 'Ожидаем игрока',
                                    heigh: 50,
                                    width: 100,
                                    onPressed: () {})
                              else
                                SizedBox.shrink()
                            else
                              gameState.game!.room.users > 2
                                  ? Spring.slide(
                                      slideType: SlideType.slide_in_left,
                                      child: Row(
                                        children: [
                                          RotatedBox(
                                            quarterTurns: 1,
                                            child: Container(
                                              width: 50.w,
                                              margin:
                                                  EdgeInsets.only(top: 20.sp),
                                              child: FlatCardFan(
                                                  children: gameMembers[1]
                                                      .cards!
                                                      .map((e) => CardWidget(
                                                            card: e
                                                                .getPlayingCard(),
                                                          ))
                                                      .toList()),
                                            ),
                                          ),
                                          Text(
                                              '${gameMembers[1].points.toString()}')
                                        ],
                                      ),
                                    )
                                  : SizedBox.shrink(),
                            if (gameState is EnteredToGame ||
                                gameState is GameFinished)
                              Spring.fadeIn(
                                child: Container(
                                  height: 50.h,
                                  color: Colors.green,
                                  padding: EdgeInsets.symmetric(
                                    vertical: 110.sp,
                                  ),
                                  child: SizedBox(
                                    child: PlayingCardView(
                                        card:
                                            gameState.game!.centralCard != null
                                                ? gameState.game!.centralCard!
                                                    .getPlayingCard()
                                                : PlayingCard(
                                                    Suit.hearts, CardValue.ace),
                                        showBack: gameState is EnteredToGame,
                                        elevation: 3.0,
                                        shape: shape),
                                  ),
                                ),
                              ),
                            gameState is EnteredToGame
                                ? gameState.game!.room.users > 3
                                    ? Button(
                                        text: gameMembers.length > 2
                                            ? '${gameMembers[2].ready ? 'Готово \n' : ''}${gameMembers[2].user.name}'
                                            : 'Ожидаем игрока',
                                        heigh: 50,
                                        width: 100,
                                        onPressed: () {})
                                    : SizedBox.shrink()
                                : gameState.game!.room.users > 3
                                    ? Spring.slide(
                                        slideType: SlideType.slide_in_right,
                                        child: Row(
                                          children: [
                                            Text(
                                                '${gameMembers[2].points.toString()}'),
                                            RotatedBox(
                                              quarterTurns: 1,
                                              child: Container(
                                                margin: EdgeInsets.only(
                                                    bottom: 20.sp),
                                                width: 50.w,
                                                child: FlatCardFan(
                                                    children: gameMembers[2]
                                                        .cards!
                                                        .map((e) => CardWidget(
                                                              card: e
                                                                  .getPlayingCard(),
                                                            ))
                                                        .toList()),
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    : SizedBox.shrink(),
                          ],
                        ),
                      ),
                      if (gameState is EnteredToGame)
                        Button(
                            text: memberUser.ready
                                ? 'Ожидаем других игроков'
                                : 'Готово',
                            heigh: 50,
                            width: 200,
                            onPressed: () {
                              BlocProvider.of<GameCubit>(context)
                                  .readyToRoom(gameState.game!);
                            })
                      else
                        Spring.slide(
                            slideType: SlideType.slide_in_bottom,
                            child: Column(
                              children: [
                                Text(memberUser.points.toString()),
                                Container(
                                    width: 70.w,
                                    padding: EdgeInsets.only(bottom: 10.sp),
                                    child: FlatCardFan(
                                        children: memberUser.cards!
                                            .map((e) => CardWidget(
                                                  card: e.getPlayingCard(),
                                                ) as Widget)
                                            .toList())),
                              ],
                            ))
                    ],
                  ),
                ));
          },
        );
      },
    );
  }
}
