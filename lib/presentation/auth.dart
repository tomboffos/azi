import 'package:azi/common/app_colors.dart';
import 'package:azi/common/app_strings.dart';
import 'package:azi/flows/lang/cubit/lang_cubit.dart';
import 'package:azi/flows/setting/cubit/setting_cubit.dart';
import 'package:azi/flows/user/cubit/user_cubit.dart';
import 'package:azi/widget/widget_button.dart';
import 'package:azi/widget/widget_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';

class AuthorizationPage extends StatefulWidget {
  AuthorizationPage({Key? key}) : super(key: key);

  @override
  _AuthorizationPageState createState() => _AuthorizationPageState();
}

class _AuthorizationPageState extends State<AuthorizationPage> {
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    BlocProvider.of<UserCubit>(context).registerUser();
    BlocProvider.of<SettingCubit>(context).getSettings();
    BlocProvider.of<LangCubit>(context).getLang();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<UserCubit, UserState>(
      listener: (context, state) {
        if (state is UserAuthorized) {
          Navigator.pushNamedAndRemoveUntil(
              context, '/homepage', (route) => false);
        }
      },
      builder: (context, state) {
        return Scaffold(
          backgroundColor: AppColors.green2,
          body: Center(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: Image.asset(
                      'assets/images/cards.png',
                      width: 60.w,
                      height: 30.h,
                    ),
                  ),
                  PageEntry(
                    obcure: false,
                    hinttext: AppString.login,
                    texttype: TextInputType.emailAddress,
                    controller: email,
                  ),
                  PageEntry(
                    obcure: true,
                    hinttext: AppString.password,
                    texttype: TextInputType.emailAddress,
                    controller: password,
                  ),
                  Button(
                      color: email.text != '' && password.text != ''
                          ? null
                          : Colors.black12,
                      text: AppString.enter,
                      heigh: MediaQuery.of(context).size.height * 0.009.h,
                      width: MediaQuery.of(context).size.width * 0.1.w,
                      onPressed: () => BlocProvider.of<UserCubit>(context)
                          .login(
                              {'email': email.text, 'password': password.text},
                              context)),
                  // TextButton(
                  //     onPressed: () =>
                  //         Navigator.pushNamed((context), '/register'),
                  //     child: Text(
                  //       AppString.register,
                  //       style: TextStyle(color: AppColors.white),
                  //     ))
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
