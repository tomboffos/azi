import 'package:azi/common/app_colors.dart';
import 'package:azi/common/app_strings.dart';
import 'package:azi/flows/user/cubit/user_cubit.dart';
import 'package:azi/flows/withdraw/cubit/withdraw_cubit.dart';
import 'package:azi/widget/widget_button.dart';
import 'package:azi/widget/widget_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:sizer/sizer.dart';

class WithdrawIndex extends StatefulWidget {
  const WithdrawIndex({Key? key}) : super(key: key);

  @override
  State<WithdrawIndex> createState() => _WithdrawIndexState();
}

class _WithdrawIndexState extends State<WithdrawIndex> {
  MaskTextInputFormatter cardFormatter = MaskTextInputFormatter(
      mask: '#### #### #### ####',
      filter: {"#": RegExp(r'[0-9]')},
      type: MaskAutoCompletionType.lazy);

  int price = 0;

  TextEditingController card = TextEditingController();
  TextEditingController name = TextEditingController();
  TextEditingController amount = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserCubit, UserState>(
      builder: (context, state) {
        if (state is! UserAuthorized)
          return Center(
            child: CircularProgressIndicator(),
          );
        return Scaffold(
          backgroundColor: AppColors.green2,
          appBar: AppBar(
            backgroundColor: AppColors.green2,
            title: Text(AppString.withdraw),
            elevation: 0,
            actions: [
              Container(
                  child: Text(
                'Ваш баланс ${state.user.balance} тг ',
                style: TextStyle(fontSize: 10.sp),
              ))
            ],
          ),
          body: SingleChildScrollView(
              child: GestureDetector(
            onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
            child: Container(
              height: 100.h,
              width: 100.w,
              child: Center(
                child: Column(
                  children: [
                    PageEntry(
                      hinttext: AppString.type_card,
                      texttype: TextInputType.number,
                      obcure: false,
                      formatter: cardFormatter,
                      controller: card,
                    ),
                    PageEntry(
                      hinttext: AppString.type_name,
                      texttype: TextInputType.text,
                      obcure: false,
                      controller: name,
                    ),
                    PageEntry(
                      controller: amount,
                      onChanged: (value) {
                        if (value != '') {
                          setState(() {
                            value = int.parse(value);

                            int percentage = (value * 0.11).abs().toInt();

                            if (percentage > 150) {
                              price = (value - (value * 0.11)).abs().toInt();
                            } else {
                              if (value < 150) {
                                price = 0;
                              } else {
                                price = (value - 150).abs().toInt();
                              }
                            }
                          });
                        } else {
                          setState(() {
                            price = 0;
                          });
                        }
                      },
                      hinttext: AppString.transfer,
                      texttype: TextInputType.number,
                      obcure: false,
                    ),
                    Column(
                      children: [
                        Text(
                          'Вам будет начислено:',
                          style: AppColors.styledText(13.sp, FontWeight.bold),
                        ),
                        SizedBox(
                          height: 10.sp,
                        ),
                        Text(
                          '$price тг',
                          style: AppColors.styledText(13.sp, FontWeight.bold),
                        )
                      ],
                    ),
                    Button(
                      color: price != 0 &&
                              card.text != '' &&
                              name.text != '' &&
                              amount.text != ''
                          ? null
                          : Colors.black12,
                      text: AppString.make_withdraw,
                      heigh: MediaQuery.of(context).size.height * 0.009.h,
                      width: MediaQuery.of(context).size.width * 0.2.w,
                      onPressed: () {
                        BlocProvider.of<WithdrawCubit>(context).makeWithdraw({
                          'amount': amount.text,
                          'card': card.text,
                          'name': name.text
                        }, context, BlocProvider.of<UserCubit>(context));

                        setState(() {
                          amount.text = '';
                          name.text = '';
                          card.text = '';
                          price = 0;
                        });
                      },
                    ),
                  ],
                ),
              ),
            ),
          )),
        );
      },
    );
  }
}
