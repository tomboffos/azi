import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:azi/common/app_colors.dart';
import 'package:azi/common/app_strings.dart';
import 'package:azi/common/app_style.dart';
import 'package:azi/flows/friend/cubit/friend_cubit.dart';
import 'package:azi/flows/setting/cubit/setting_cubit.dart';
import 'package:azi/flows/user/cubit/user_cubit.dart';
import 'package:azi/presentation/components/friend_drawer.dart';
import 'package:azi/services/localization/app_localization.dart';
import 'package:azi/services/modal_service.dart';
import 'package:azi/widget/widget_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';
import 'package:spring/spring.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

bool sound = true;

class _HomePageState extends State<HomePage> {
  final audioPlayer = AudioPlayer();
  // @override
  // void dispose() {
  //   audioplayer.dispose();
  //   super.dispose();
  // }

  @override
  void initState() {
    // TODO: implement initState
    BlocProvider.of<FriendCubit>(context).getFriends();
    BlocProvider.of<FriendCubit>(context).getOffers();
    BlocProvider.of<FriendCubit>(context).getRequests();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final cubit = BlocProvider.of<SettingCubit>(context);
    return BlocConsumer<UserCubit, UserState>(
      listener: (context, state) {
        if (state is UserInitial) {
          Navigator.pushNamedAndRemoveUntil(context, '/', (route) => false);
        }
      },
      builder: (context, state) {
        if (state is! UserAuthorized) return SizedBox.shrink();
        return Scaffold(
          backgroundColor: AppColors.green2,
          drawer: FriendsDrawer(),
          appBar: AppBar(
            backgroundColor: AppColors.green2,
            elevation: 0,
            title: state.user.email != null
                ? Container(
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    width: MediaQuery.of(context).size.width * 0.95,
                    child: Text(
                      state.user.email!,
                      textAlign: TextAlign.center,
                    ),
                  )
                : SizedBox.shrink(),
            actions: [
              // Container(
              //   child: IconButton(
              //     onPressed: () async {
              //       setState(() {
              //         sound = !sound;
              //       });
              //     },
              //     icon: Icon(
              //       (sound == false)
              //           ? Icons.music_note_outlined
              //           : Icons.music_off_outlined,
              //       size: 30.sp,
              //       color: AppColors.orange,
              //     ),
              //   ),
              // ),
              // IconButton(
              //   onPressed: () =>
              //       Navigator.pushNamed((context), '/notification-page'),
              //   icon: Icon(
              //     Icons.notifications_none_sharp,
              //     size: 30.sp,
              //   ),
              // ),
              IconButton(
                onPressed: () => Navigator.pushNamed((context), '/settings',
                    arguments: state.user),
                icon: Icon(
                  Icons.settings_outlined,
                  size: 30.sp,
                ),
              )
            ],
          ),
          body: RefreshIndicator(
            onRefresh: () async {
              BlocProvider.of<UserCubit>(context).getUser();
              return null;
            },
            child: SingleChildScrollView(
              child: Container(
                height: 100.h,
                child: Center(
                  child: Spring.slide(
                    slideType: SlideType.slide_in_top,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        if (cubit.getSetting('payable') == 'true')
                          Button(
                            text:
                                '${AppLocalizations.of(context)!.translate('balance')} ${state.user.balance} ₸',
                            heigh: MediaQuery.of(context).size.height / 2.h,
                            width: MediaQuery.of(context).size.width * 0.2.w,
                            onPressed: () {
                              state.user.email != null
                                  ? Navigator.pushNamed(
                                      (context),
                                      '/payment',
                                    )
                                  : ModalService().showErrorMessage(
                                      'Пожалуйста зарегистрируйтесь', context);
                            },
                          ),
                        Container(
                          // margin: EdgeInsets.only(right: 10),
                          width: MediaQuery.of(context).size.width * 0.2.w,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: AppColors.black,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                            ),
                            onPressed: () {
                              // Кидает в пэйбокс оплату
                            },
                            child: Container(
                              width: MediaQuery.of(context).size.width / 0.2.w,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'Сумма коинов : ${state.user.fakeBalance}',
                                    textAlign: TextAlign.center,
                                  ),
                                  Icon(
                                    Icons.token_rounded,
                                    color: AppColors.red,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.symmetric(vertical: 20),
                          child: Image.asset(
                            'assets/images/cards.png',
                            width: 60.w,
                            height: 20.h,
                          ),
                        ),
                        Button(
                          text: AppString.play,
                          width: MediaQuery.of(context).size.width * 0.1.w,
                          onPressed: () => Navigator.pushNamed(
                              (context), '/rooms',
                              arguments: 1),
                          heigh: MediaQuery.of(context).size.height * 0.006.h,
                        ),
                        if (cubit.getSetting('payable') == 'true')
                          Button(
                            text: AppString.play_pay,
                            width: MediaQuery.of(context).size.width * 0.1.w,
                            onPressed: () => state.user.email != null
                                ? Navigator.pushNamed((context), '/rooms',
                                    arguments: 2)
                                : ModalService().showErrorMessage(
                                    'Пожалуйста зарегистрируйтесь', context),
                            heigh: MediaQuery.of(context).size.height * 0.006.h,
                          ),
                        Button(
                            text: AppString.instrusction,
                            heigh: MediaQuery.of(context).size.height * 0.006.h,
                            width: MediaQuery.of(context).size.width * 0.1.w,
                            onPressed: () {
                              Navigator.pushNamed(context, '/instruction');
                            }),
                        if (cubit.getSetting('payable') == 'true')
                          Button(
                            text: AppString.bank,
                            width: MediaQuery.of(context).size.width * 0.1.w,
                            onPressed: () => state.user.email != null
                                ? Navigator.pushNamed(
                                    (context),
                                    '/transfer',
                                  )
                                : ModalService().showErrorMessage(
                                    'Пожалуйста зарегистрируйтесь', context),
                            heigh: MediaQuery.of(context).size.height * 0.006.h,
                          ),
                        Container(
                          margin: const EdgeInsets.symmetric(vertical: 10),
                          width: MediaQuery.of(context).size.width * 0.1.w,
                          height: MediaQuery.of(context).size.height * 0.006.h,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: AppColors.red,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12),
                              ),
                            ),
                            onPressed: () {
                              if (Platform.isAndroid) {
                                SystemNavigator.pop();
                              } else if (Platform.isIOS) {
                                exit(0);
                              }
                            },
                            child: Text(
                              AppString.exit,
                              style: AppTextStyles.menu,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
