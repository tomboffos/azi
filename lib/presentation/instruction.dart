import 'package:azi/common/app_colors.dart';
import 'package:azi/common/app_strings.dart';
import 'package:azi/flows/setting/cubit/setting_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';

class InstructionIndex extends StatelessWidget {
  const InstructionIndex({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cubit = BlocProvider.of<SettingCubit>(context);
    return Scaffold(
      backgroundColor: AppColors.green2,
      appBar: AppBar(
        backgroundColor: AppColors.green2,
        title: Text(AppString.instrusction),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 2.h, horizontal: 5.w),
          child: Center(
            child: Text(
              cubit.getSetting('instructions'),
              style: AppColors.styledText(17.0, FontWeight.w400),
            ),
          ),
        ),
      ),
    );
  }
}
