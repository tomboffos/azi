import 'package:azi/common/app_colors.dart';
import 'package:azi/common/app_strings.dart';
import 'package:azi/widget/widget_button.dart';
import 'package:azi/widget/widget_textfield.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class TransferPage extends StatefulWidget {
  TransferPage({Key? key}) : super(key: key);

  @override
  _TransferPageState createState() => _TransferPageState();
}

class _TransferPageState extends State<TransferPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.green2,
      appBar: AppBar(
        backgroundColor: AppColors.green2,
        title: Text(AppString.bank),
        elevation: 0,
      ),
      body: Center(
        child: Column(
          children: [
            // Button(
            //     text: AppString.summ,
            //     heigh: MediaQuery.of(context).size.height / 2.h,
            //     width: MediaQuery.of(context).size.width * 0.2.w,
            //     onPressed: () {
            //       // kidaet na paybox
            //     }),
            // PageEntry(
            //   hinttext: AppString.transfer,
            //   texttype: TextInputType.number,
            //   obcure: false,
            // ),
            // Icon(
            //   Icons.transform_rounded,
            //   color: AppColors.black,
            //   size: 100.sp,
            // ),
            // PageEntry(
            //   sufficicon: Icon(
            //     Icons.token_rounded,
            //     color: Colors.red,
            //   ),
            //   hinttext: AppString.transfer2,
            //   texttype: TextInputType.number,
            //   obcure: false,
            // ),
            Button(
              text: AppString.pay,
              heigh: MediaQuery.of(context).size.height * 0.009.h,
              width: MediaQuery.of(context).size.width * 0.2.w,
              onPressed: () {
                Navigator.pushNamed(context, '/payment');
              },
            ),
            Button(
              text: AppString.transfermoney,
              heigh: MediaQuery.of(context).size.height * 0.009.h,
              width: MediaQuery.of(context).size.width * 0.2.w,
              onPressed: () {
                Navigator.pushNamed(context, '/withdraw');
              },
            ),
          ],
        ),
      ),
    );
  }
}
