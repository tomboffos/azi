import 'package:azi/common/app_colors.dart';
import 'package:azi/common/app_strings.dart';
import 'package:azi/data/model/user.dart';
import 'package:azi/flows/lang/cubit/lang_cubit.dart';
import 'package:azi/flows/user/cubit/user_cubit.dart';
import 'package:azi/widget/widget_button.dart';
import 'package:azi/widget/widget_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';

class SettingPage extends StatefulWidget {
  final User user;
  SettingPage({Key? key, required this.user}) : super(key: key);

  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  TextEditingController name = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    if (widget.user.name != null) name.text = widget.user.name!;
    if (widget.user.email != null) email.text = widget.user.email!;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<UserCubit, UserState>(
      listener: (context, state) {},
      builder: (context, state) {
        return Scaffold(
          backgroundColor: AppColors.green2,
          appBar: AppBar(
            backgroundColor: AppColors.green2,
            title: Text(AppString.settings),
            elevation: 0,
          ),
          body: SingleChildScrollView(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  PageEntry(
                    obcure: false,
                    controller: name,
                    hinttext: AppString.login1,
                    texttype: TextInputType.emailAddress,
                  ),
                  PageEntry(
                    obcure: false,
                    controller: email,
                    hinttext: AppString.email,
                    texttype: TextInputType.emailAddress,
                  ),
                  PageEntry(
                    obcure: true,
                    controller: password,
                    hinttext: AppString.password,
                    texttype: TextInputType.emailAddress,
                  ),
                  BlocBuilder<LangCubit, LangState>(
                    builder: (context, state) {
                      return Container(
                        padding: EdgeInsets.only(left: 10.sp),
                        margin: EdgeInsets.only(top: 5.sp),
                        width: 75.w,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10)),
                        child: DropdownButton(
                          style: TextStyle(fontSize: 15.0, color: Colors.black),
                          elevation: 0,
                          isExpanded: true,
                          hint: const Text('Выберите язык'),
                          underline: const SizedBox.shrink(),
                          value: state.locale,
                          items: const [
                            DropdownMenuItem(
                              child: Text('Руский'),
                              value: Locale('ru'),
                            ),
                            DropdownMenuItem(
                              child: Text('English'),
                              value: Locale('en'),
                            ),
                            DropdownMenuItem(
                              child: Text('Казах'),
                              value: Locale('fr'),
                            ),
                          ],
                          onChanged: (value) {
                            BlocProvider.of<LangCubit>(context)
                                .saveLang(value as Locale);
                          },
                        ),
                      );
                    },
                  ),
                  Button(
                    text: AppString.save,
                    heigh: MediaQuery.of(context).size.height * 0.008.h,
                    width: MediaQuery.of(context).size.width * 0.19.w,
                    onPressed: () {
                      BlocProvider.of<UserCubit>(context).updateUser({
                        'name': name.text,
                        'email': email.text,
                        'password': password.text
                      }, context);
                    },
                  ),
                  Button(
                    text: AppString.loginout,
                    heigh: MediaQuery.of(context).size.height * 0.008.h,
                    width: MediaQuery.of(context).size.width * 0.19.w,
                    onPressed: () {
                      Navigator.pushNamed(context, '/');
                    },
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
