class Room {
  int id;
  int users;
  int bid;
  int roomTypeId;
  int currentUsers;

  Room.fromJson(Map json)
      : id = json['id'],
        users = json['users'],
        bid = json['bid'],
        currentUsers = json['current_users'],
        roomTypeId = json['room_type_id'];
}
