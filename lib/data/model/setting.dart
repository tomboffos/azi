class Setting {
  String key;
  String value;

  Setting.fromJson(Map json)
      : key = json['key'],
        value = json['value'];
}
