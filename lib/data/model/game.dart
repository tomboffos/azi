import 'package:azi/data/model/card.dart';
import 'package:azi/data/model/game_member.dart';
import 'package:azi/data/model/room.dart';
import 'package:azi/data/model/user.dart';

class Game {
  int? bid;
  String? startedAt;
  String? finishedAt;
  Room room;
  List<GameMember> members;
  int id;
  GameMember? user;
  Card? centralCard;

  Game.fromJson(Map json)
      : bid = json['bid'],
        startedAt = json['started_at'],
        finishedAt = json['finished_at'],
        room = Room.fromJson(json['room_id']),
        members = json['members']
            .map((e) {
              return GameMember.fromJson(e);
            })
            .toList()
            .cast<GameMember>(),
        id = json['id'],
        user = json['user'] != null ? GameMember.fromJson(json['user']) : null,
        centralCard = json['central_card'] != null
            ? Card.fromJson(json['central_card'])
            : null;
}
