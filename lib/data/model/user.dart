class User {
  String? name;
  String? email;
  int balance;
  int fakeBalance;
  int id;
  User.fromJson(Map json)
      : name = json['name'],
        email = json['email'],
        balance = json['balance'],
        fakeBalance = json['fake_balance'],
        id = json['id'];
}
