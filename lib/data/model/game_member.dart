import 'dart:convert';

import 'package:azi/data/model/card.dart';
import 'package:azi/data/model/user.dart';

class GameMember {
  User user;
  List<Card>? cards;
  int? points;
  bool ready;

  GameMember.fromJson(Map json)
      : user = User.fromJson(json['user']),
        cards = json['cards'] != null
            ? jsonDecode(json['cards'])
                .map((e) {
                  return Card.fromJson(e);
                })
                .toList()
                .cast<Card>()
            : null,
        points = json['points'],
        ready = json['ready'];
}
