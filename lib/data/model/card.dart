import 'package:playing_cards/playing_cards.dart';

class Card {
  String seniority;
  String suit;

  Card.fromJson(Map json)
      : seniority = json['seniority'],
        suit = json['suit'];

  PlayingCard getPlayingCard() {
    return PlayingCard(processSuits(suit), processSeniority(seniority));
  }

  CardValue processSeniority(seniority) {
    switch (seniority) {
      case 'ace':
        return CardValue.ace;
      case 'king':
        return CardValue.king;
      case 'queen':
        return CardValue.queen;
      case 'jack':
        return CardValue.jack;
      case '10':
        return CardValue.ten;
      default:
        return CardValue.ace;
    }
  }

  Suit processSuits(suit) {
    switch (suit) {
      case 'spade':
        return Suit.spades;
      case 'clubs':
        return Suit.clubs;
      case 'hearts':
        return Suit.hearts;
      case 'diamonds':
        return Suit.diamonds;
      default:
        return Suit.spades;
    }
  }
}
