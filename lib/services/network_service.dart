import 'package:flutter/foundation.dart';
import 'package:http/http.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

@injectable
class NetworkService {
  static const apiUrl =
      kDebugMode ? 'http://127.0.0.1:8000/api' : 'https://azi.it-lead.net/api';
  static const apiGetUrl = 'www.azi.it-lead.net/api';

  Future<Response> unauthorizedRequest(String additionalUrl, String method,
      {bool withMetaData = false,
      Map<String, String>? headers,
      Map<String, dynamic>? data}) async {
    if (headers != null) {
      headers.addAll({'Accept': 'application/json'});
    } else {
      headers = {'Accept': 'application/json'};
    }

    switch (method) {
      case 'GET':
        final response = await get(
          Uri.parse(apiUrl + additionalUrl).replace(queryParameters: data),
          headers: headers,
        );
        return response;
      case 'POST':
        final response = await post(Uri.parse(apiUrl + additionalUrl),
            body: data, headers: headers);

        return response;

      default:
        final response = await get(
          Uri.http(apiUrl, additionalUrl, data),
        );
        return response;
    }
  }

  Future<Response?> authorizedRequest(String additionalUrl,
      Map<String, dynamic> data, Map<String, String> headers, String method,
      {bool withMetaData = false}) async {
    final token = (await SharedPreferences.getInstance()).getString('token');

    if (token == null) return null;

    headers.addAll({'Authorization': 'Bearer $token'});
    switch (method) {
      case 'GET':
        final response = await get(
          Uri.parse(apiUrl + additionalUrl).replace(queryParameters: data),
          headers: headers,
        );
        return response;
      case 'POST':
        final response = await post(Uri.parse(apiUrl + additionalUrl),
            body: data, headers: headers);

        return response;

      case 'PUT':
        final response = await put(Uri.parse(apiUrl + additionalUrl),
            body: data, headers: headers);

        return response;

      case 'DELETE':
        final response = await delete(Uri.parse(apiUrl + additionalUrl),
            body: data, headers: headers);

        return response;
      default:
        final response = await get(
          Uri.http(apiUrl, additionalUrl, data),
        );
        return response;
    }
  }
}
