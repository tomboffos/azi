import 'package:azi/common/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

@injectable
class ModalService {
  ScaffoldFeatureController showSuccessMessage(
      String text, BuildContext context) {
    return ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(text),
      backgroundColor: Colors.greenAccent,
    ));
  }

  showErrorMessage(String text, BuildContext context) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(
        text,
        style: const TextStyle(color: Colors.white),
      ),
      backgroundColor: Colors.red,
    ));
  }

  showMainDialog(BuildContext context, Widget content) {
    showDialog(
      barrierColor: Colors.black38,
      context: context,
      barrierDismissible: false,
      builder: (context) => AlertDialog(
        backgroundColor: Colors.transparent,
        content: content,
        elevation: 0.0,
      ),
    );
  }
}
