import 'package:azi/flows/app/cubit/app_cubit.dart';
import 'package:azi/flows/friend/cubit/friend_cubit.dart';
import 'package:azi/flows/game/cubit/game_cubit.dart';
import 'package:azi/flows/lang/cubit/lang_cubit.dart';
import 'package:azi/flows/payment/cubit/payment_cubit.dart';
import 'package:azi/flows/room/cubit/room_cubit.dart';
import 'package:azi/flows/setting/cubit/setting_cubit.dart';
import 'package:azi/flows/user/cubit/user_cubit.dart';
import 'package:azi/injection.dart';
import 'package:azi/router.dart';
import 'package:azi/services/localization/app_localization_setup.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key, required this.router}) : super(key: key);
  final AppRouter router;

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (BuildContext context, Orientation orientation,
              DeviceType deviceType) =>
          MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => getIt.get<AppCubit>(),
          ),
          BlocProvider(
            create: (context) => getIt.get<LangCubit>(),
          ),
          BlocProvider(
            create: (context) => getIt.get<UserCubit>(),
          ),
          BlocProvider(
            create: (context) => getIt.get<RoomCubit>(),
          ),
          BlocProvider(
            create: (context) => getIt.get<GameCubit>(),
          ),
          BlocProvider(
            create: (context) => getIt.get<SettingCubit>(),
          ),
          BlocProvider(
            create: (context) => getIt.get<PaymentCubit>(),
          ),
          BlocProvider(
            create: (context) => getIt.get<FriendCubit>(),
          ),
        ],
        child: BlocBuilder<LangCubit, LangState>(
          builder: (context, state) {
            return MaterialApp(
              title: '10 Shamai',
              debugShowCheckedModeBanner: false,
              localizationsDelegates:
                  AppLocalizationsSetup.localizationsDelegates,
              supportedLocales: AppLocalizationsSetup.supportedLocales,
              locale: state.locale,
              theme: ThemeData(),
              onGenerateRoute: router.generateRouter,
            );
          },
        ),
      ),
    );
  }
}
