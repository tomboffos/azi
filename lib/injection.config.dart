// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import 'flows/friend/cubit/friend_cubit.dart' as _i25;
import 'flows/friend/repository/friend_repository.dart' as _i16;
import 'flows/friend/repository/friend_repository_impl.dart' as _i17;
import 'flows/game/cubit/game_cubit.dart' as _i26;
import 'flows/game/repository/game_repository.dart' as _i18;
import 'flows/game/repository/game_repository_impl.dart' as _i19;
import 'flows/lang/cubit/lang_cubit.dart' as _i3;
import 'flows/payment/cubit/payment_cubit.dart' as _i20;
import 'flows/payment/repository/payment_repository.dart' as _i6;
import 'flows/payment/repository/payment_repository_impl.dart' as _i7;
import 'flows/room/cubit/room_cubit.dart' as _i21;
import 'flows/room/repository/room_repository.dart' as _i8;
import 'flows/room/repository/room_repository_impl.dart' as _i9;
import 'flows/setting/cubit/setting_cubit.dart' as _i22;
import 'flows/setting/repository/interface/setting_repository.dart' as _i10;
import 'flows/setting/repository/setting_repository_impl.dart' as _i11;
import 'flows/user/cubit/user_cubit.dart' as _i23;
import 'flows/user/repository/user_repository.dart' as _i12;
import 'flows/user/repository/user_repository_impl.dart' as _i13;
import 'flows/withdraw/cubit/withdraw_cubit.dart' as _i24;
import 'flows/withdraw/repository/withdraw_repository.dart' as _i14;
import 'flows/withdraw/repository/withdraw_repository_impl.dart' as _i15;
import 'services/modal_service.dart' as _i4;
import 'services/network_service.dart'
    as _i5; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  gh.factory<_i3.LangCubit>(() => _i3.LangCubit());
  gh.factory<_i4.ModalService>(() => _i4.ModalService());
  gh.factory<_i5.NetworkService>(() => _i5.NetworkService());
  gh.factory<_i6.PaymentRepository>(
      () => _i7.PaymentRepositoryImpl(get<_i5.NetworkService>()));
  gh.factory<_i8.RoomRepository>(
      () => _i9.RoomRepositoryImpl(get<_i5.NetworkService>()));
  gh.factory<_i10.SettingRepository>(() => _i11.SettingRepositoryImpl(
      get<_i5.NetworkService>(), get<_i4.ModalService>()));
  gh.factory<_i12.UserRepository>(
      () => _i13.UserRepositoryImpl(get<_i5.NetworkService>()));
  gh.factory<_i14.WithdrawRepository>(() => _i15.WithdrawRepositoryImpl(
      get<_i4.ModalService>(), get<_i5.NetworkService>()));
  gh.factory<_i16.FriendRepository>(
      () => _i17.FriendRepositoryImpl(get<_i5.NetworkService>()));
  gh.factory<_i18.GameRepository>(
      () => _i19.GameRepositoryImpl(get<_i5.NetworkService>()));
  gh.factory<_i20.PaymentCubit>(
      () => _i20.PaymentCubit(get<_i6.PaymentRepository>()));
  gh.factory<_i21.RoomCubit>(() => _i21.RoomCubit(get<_i8.RoomRepository>()));
  gh.factory<_i22.SettingCubit>(
      () => _i22.SettingCubit(get<_i10.SettingRepository>()));
  gh.factory<_i23.UserCubit>(() => _i23.UserCubit(get<_i12.UserRepository>()));
  gh.factory<_i24.WithdrawCubit>(
      () => _i24.WithdrawCubit(get<_i14.WithdrawRepository>()));
  gh.factory<_i25.FriendCubit>(
      () => _i25.FriendCubit(get<_i16.FriendRepository>()));
  gh.factory<_i26.GameCubit>(() => _i26.GameCubit(get<_i18.GameRepository>()));
  return get;
}
