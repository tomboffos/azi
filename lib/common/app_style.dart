import 'package:azi/common/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppTextStyles {
  static TextStyle menu = GoogleFonts.kdamThmor(
    fontSize: 15,
    color: AppColors.white,
    fontWeight: FontWeight.w900,
  );
  static TextStyle rooms = GoogleFonts.kdamThmor(
    fontSize: 15,
    color: AppColors.black,
    fontWeight: FontWeight.w900,
  );
}
