class AppString {
  // auth page
  static const enter = 'Войти';
  static const register = 'Зарегистрироваться';
  static const login = 'Напишите свой логин или почту ';
  static const password = 'Напиште свой пароль';
  // register page
  static const email = 'Введите почту';
  static const login1 = 'Введите уникальный логин';
  // home page and play rooms
  static const loginname = 'royaledman@gmail.com';
  // setting page
  static const settings = 'Настройки';
  static const save = 'Сохранить';
  static const play = 'Играть';
  static const play_pay = 'Игра на деньги';
  static const instrusction = 'Инструкция';
  static const bank = 'Баланс';
  static const withdraw = 'Вывод денег';
  static const loginout = 'Войти в другой аккаунт';
  // transfer page
  static const transfer = '1000 ₸';
  static const transfer2 = '1000';
  static const transfermoney = 'Вывод денег';
  // notication page
  static const notification_page = 'Уведомление';
  static const notification_message = 'На ваш счет поступило : 20000 ₸';
  static const exit = 'Выйти';
  static const summ = 'Сумма денег на счету : 1000 ₸';

  static const summ2 = '4 Игрока Сумма : 5000 ₸';
  static const aldicoin = 'Сумма коинов :10000';
  // transfer
  static const make_transfer = 'Сделать перевод';
  static const make_withdraw = 'Сделать вывод';

  static const type_card = 'Введите ваш номер карты';
  static const type_name = 'Введите указанное имя на карте';
  static const type_price = 'Введите сумму к пополнению';
  static const pay = 'Пополнить баланс';
}
