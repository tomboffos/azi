import 'package:azi/data/model/user.dart';
import 'package:azi/flows/friend/repository/friend_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

part 'friend_state.dart';

@injectable
class FriendCubit extends Cubit<FriendState> {
  final FriendRepository friendRepository;
  FriendCubit(this.friendRepository) : super(FriendInitial());

  List<User> friends = [];
  List<User> requests = [];
  List<User> offers = [];

  getFriends({String? search}) async {
    final fetchedFriends = await friendRepository.getFriends(search: search);
    if (fetchedFriends != null) {
      friends = fetchedFriends;
      emit(FriendInitial());
    }
  }

  getOffers({String? search}) async {
    final fetchedOffers = await friendRepository.getOffers(search: search);

    if (fetchedOffers != null) {
      offers = fetchedOffers;
      emit(FriendInitial());
    }
  }

  getRequests({String? search}) async {
    final fetchedRequests = await friendRepository.getRequests(search: search);

    if (fetchedRequests != null) {
      requests = fetchedRequests;

      emit(FriendInitial());
    }
  }
}
