part of 'friend_cubit.dart';

@immutable
abstract class FriendState {}

class FriendInitial extends FriendState {}

class FriendLoaded extends FriendState {
  final List<User> friends;

  FriendLoaded({required this.friends});
}
