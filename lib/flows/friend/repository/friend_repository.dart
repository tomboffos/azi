import 'package:azi/data/model/user.dart';

abstract class FriendRepository {
  Future<List<User>?> getFriends({String? search}) async {}

  Future<List<User>?> getRequests({String? search}) async {}

  Future<List<User>?> getOffers({String? search}) async {}
}
