import 'dart:convert';

import 'package:azi/data/model/user.dart';
import 'package:azi/flows/friend/repository/friend_repository.dart';
import 'package:azi/services/network_service.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: FriendRepository)
class FriendRepositoryImpl implements FriendRepository {
  final NetworkService networkService;

  FriendRepositoryImpl(this.networkService);

  @override
  Future<List<User>?> getFriends({String? search}) async {
    final response = await networkService.authorizedRequest(
        '/friends',
        {'search': search},
        {'Accept': "application/json", 'Content': 'application/json'},
        'GET');
    if (response!.statusCode == 200) {
      return jsonDecode(response.body)['data']
          .map((e) => User.fromJson(e['friend']))
          .toList()
          .cast<User>();
    }
  }

  @override
  Future<List<User>?> getOffers({String? search}) async {
    final response = await networkService.authorizedRequest(
        '/offers',
        {'search': search},
        {'Accept': "application/json", 'Content': 'application/json'},
        'GET');
    if (response!.statusCode == 200)
      return jsonDecode(response.body)['data']
          .map((e) => User.fromJson(e['friend']))
          .toList()
          .cast<User>();
  }

  @override
  Future<List<User>?> getRequests({String? search}) async {
    // TODO: implement getRequests
    final response = await networkService.authorizedRequest(
        '/requests',
        {'search': search},
        {'Accept': "application/json", 'Content': 'application/json'},
        'GET');
    if (response!.statusCode == 200)
      return jsonDecode(response.body)['data']
          .map((e) => User.fromJson(e['friend']))
          .toList()
          .cast<User>();
  }
}
