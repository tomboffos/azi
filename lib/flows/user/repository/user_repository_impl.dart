import 'dart:convert';

import 'package:azi/data/model/user.dart';
import 'package:azi/flows/user/repository/user_repository.dart';
import 'package:azi/services/modal_service.dart';
import 'package:azi/services/network_service.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

@Injectable(as: UserRepository)
class UserRepositoryImpl implements UserRepository {
  final NetworkService networkService;

  UserRepositoryImpl(this.networkService);

  @override
  Future<User?> registerUser(Map<String, dynamic> data) async {
    final deviceToken =
        (await SharedPreferences.getInstance()).get('device_token');

    data.addAll({'device': deviceToken});

    final response = await networkService.authorizedRequest(
        '/auth/registration', data, {'Accept': 'application/json'}, 'POST');

    if (response != null) {
      final mainData = jsonDecode(response.body);

      (await SharedPreferences.getInstance())
          .setString('token', mainData['token']);

      return User.fromJson(mainData['user']);
    }
    return null;
  }

  @override
  Future<User?> registerToken() async {
    final deviceToken =
        (await SharedPreferences.getInstance()).get('device_token');

    final response = await networkService.unauthorizedRequest(
        '/auth/first-register', 'POST',
        data: {'device': deviceToken});
    final data = jsonDecode(response.body);

    (await SharedPreferences.getInstance()).setString('token', data['token']);

    return User.fromJson(data['user']);
  }

  @override
  Future<User?> loginUser(
      Map<String, String> data, BuildContext context) async {
    final deviceToken =
        (await SharedPreferences.getInstance()).getString('device_token');
    if (deviceToken != null) {
      data['device'] = deviceToken;
    }
    final response = await networkService
        .unauthorizedRequest('/auth/login', 'POST', data: data);

    final body = jsonDecode(response.body);

    if (response.statusCode == 400) {
      ModalService().showErrorMessage(body['message'], context);
      return null;
    }

    (await SharedPreferences.getInstance()).setString('token', body['token']);

    return User.fromJson(body['user']);
  }
}
