import 'package:azi/data/model/user.dart';
import 'package:flutter/material.dart';

abstract class UserRepository {
  Future<User?> registerToken() async {}

  Future<User?> registerUser(Map<String, dynamic> data) async {}

  Future<User?> loginUser(
      Map<String, String> data, BuildContext context) async {}
}
