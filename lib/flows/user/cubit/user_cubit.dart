import 'package:azi/data/model/user.dart';
import 'package:azi/flows/user/repository/user_repository.dart';
import 'package:azi/services/modal_service.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

part 'user_state.dart';

@injectable
class UserCubit extends Cubit<UserState> {
  final UserRepository userRepository;

  UserCubit(this.userRepository) : super(UserInitial());

  registerUser() async {
    if (state is UserInitial) {
      User? user = await userRepository.registerToken();

      if (user != null) emit(UserAuthorized(user));
    }
  }

  getUser() async {
    User? user = await userRepository.registerToken();

    if (user != null) emit(UserAuthorized(user));
  }

  updateUser(Map<String, dynamic> data, BuildContext context) async {
    User? user = await userRepository.registerUser(data);

    if (user != null) {
      ModalService().showSuccessMessage('Успешно обновлено', context);
      emit(UserAuthorized(user));
    }
  }

  login(Map<String, String> data, BuildContext context) async {
    User? user = await userRepository.loginUser(data, context);

    if (user != null) {
      emit(UserAuthorized(user));
      Navigator.pushNamedAndRemoveUntil(context, '/home', (route) => false);
    }
  }
}
