part of 'user_cubit.dart';

@immutable
abstract class UserState {}

class UserInitial extends UserState {}

class UserAuthorized extends UserState {
  final User user;

  UserAuthorized(this.user);
}
