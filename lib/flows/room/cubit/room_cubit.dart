import 'package:azi/data/model/room.dart';
import 'package:azi/flows/room/repository/room_repository.dart';

import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:socket_io_client/socket_io_client.dart';

part 'room_state.dart';

@injectable
class RoomCubit extends Cubit<RoomState> {
  final RoomRepository roomRepository;
  RoomCubit(this.roomRepository) : super(RoomInitial());

  Socket socket = io(
      'https://socket.it-lead.net',
      OptionBuilder()
          .setTransports(['websocket'])
          .enableForceNewConnection()
          .build());

  List<int>? bids = [];

  fetchBids(int roomTypeId) async {
    bids = await roomRepository.bids(roomTypeId);

    emit(RoomInitial());
  }

  List<Room>? rooms = [];

  fetchRoomsByBids(int bid, int roomTypeId) async {
    rooms = await roomRepository.fetchRooms(bid, roomTypeId);

    addListenerOfChangingRooms(rooms);

    emit(RoomInitial());
  }

  addListenerOfChangingRooms(List<Room>? rooms) {
    socket.connect();
    socket.on('azi.room.changed', (data) {
      Room room = Room.fromJson(data);
      if (rooms != null) {
        if (rooms.map((e) => e.id == room.id).isNotEmpty) {
          rooms.removeWhere((element) => element.id == room.id);
          rooms.insert(0, room);
        }

        emit(RoomInitial());
      }
    });
  }

  Future enteringToRoom() async {
    emit(RoomLoading());
    Future.delayed(Duration(seconds: 3), () {
      emit(RoomInitial());
    });
  }
}
