part of 'room_cubit.dart';

@immutable
abstract class RoomState {}

class RoomInitial extends RoomState {}

class RoomLoading extends RoomState {}
