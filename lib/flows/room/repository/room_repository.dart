import 'package:azi/data/model/room.dart';

abstract class RoomRepository {
  Future<List<int>?> bids(int roomTypeId) async {}

  Future<List<Room>?> fetchRooms(int bid, int roomTypeId) async {}
}
