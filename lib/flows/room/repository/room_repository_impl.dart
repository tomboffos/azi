import 'dart:convert';

import 'package:azi/data/model/room.dart';
import 'package:azi/flows/room/repository/room_repository.dart';
import 'package:azi/services/network_service.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: RoomRepository)
class RoomRepositoryImpl implements RoomRepository {
  final NetworkService networkService;

  RoomRepositoryImpl(this.networkService);
  @override
  Future<List<int>?> bids(int roomTypeId) async {
    final response = await networkService.unauthorizedRequest(
        '/bids?room_type_id=$roomTypeId', 'GET');

    return jsonDecode(response.body).map((e) => e).toList().cast<int>();
  }

  @override
  Future<List<Room>?> fetchRooms(int bid, int roomTypeId) async {
    final response = await networkService.authorizedRequest(
        '/room',
        {'bid': bid.toString(), 'room_type_id': roomTypeId.toString()},
        {'Accept': 'application/json'},
        'GET');

    return jsonDecode(response!.body)['data']
        .map((e) => Room.fromJson(e))
        .toList()
        .cast<Room>();
  }
}
