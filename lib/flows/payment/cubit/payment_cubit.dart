import 'package:azi/flows/payment/repository/payment_repository.dart';
import 'package:azi/flows/user/cubit/user_cubit.dart';
import 'package:azi/services/modal_service.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

part 'payment_state.dart';

@injectable
class PaymentCubit extends Cubit<PaymentState> {
  final PaymentRepository paymentRepository;
  PaymentCubit(this.paymentRepository) : super(PaymentInitial());

  getLink(String amount, BuildContext context) async {
    final link = await paymentRepository.generateLink(amount);

    if (link != null) {
      Navigator.pushNamed(context, '/payment/process', arguments: link);
    }
  }

  paymentListener(String url, BuildContext context, UserCubit userCubit) {
    if (url.contains('success')) {
      ModalService().showSuccessMessage('Оплата Прошла успешна', context);
      Navigator.pushNamedAndRemoveUntil(context, '/homepage', (route) => false);

      userCubit.getUser();
    }
    if (url.contains('fail')) {
      ModalService().showErrorMessage('К сожалению произошла ошибка', context);
      Navigator.pop(context);
    }
  }
}
