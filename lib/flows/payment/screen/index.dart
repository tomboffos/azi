import 'package:azi/common/app_colors.dart';
import 'package:azi/common/app_strings.dart';
import 'package:azi/flows/payment/cubit/payment_cubit.dart';
import 'package:azi/flows/user/cubit/user_cubit.dart';
import 'package:azi/widget/widget_button.dart';
import 'package:azi/widget/widget_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';

class PaymentIndex extends StatefulWidget {
  PaymentIndex({Key? key}) : super(key: key);

  @override
  State<PaymentIndex> createState() => _PaymentIndexState();
}

class _PaymentIndexState extends State<PaymentIndex> {
  TextEditingController amount = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserCubit, UserState>(
      builder: (context, state) {
        return Scaffold(
          backgroundColor: AppColors.green2,
          appBar: AppBar(
            backgroundColor: AppColors.green2,
            title: Text(AppString.pay),
            elevation: 0,
          ),
          body: SingleChildScrollView(
              child: Center(
            child: Column(
              children: [
                PageEntry(
                  hinttext: AppString.type_price,
                  texttype: TextInputType.number,
                  obcure: false,
                  onChanged: (value) {
                    setState(() {});
                  },
                  controller: amount,
                ),
                Button(
                  color: amount.text != '' ? null : Colors.black12,
                  text: AppString.pay,
                  heigh: MediaQuery.of(context).size.height * 0.009.h,
                  width: MediaQuery.of(context).size.width * 0.2.w,
                  onPressed: () {
                    // Navigator.pushNamed(context, '/payment');
                    if (amount.text != '')
                      BlocProvider.of<PaymentCubit>(context)
                          .getLink(amount.text, context);
                  },
                ),
              ],
            ),
          )),
        );
      },
    );
  }
}
