import 'package:azi/flows/payment/cubit/payment_cubit.dart';
import 'package:azi/flows/user/cubit/user_cubit.dart';
import 'package:azi/services/modal_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PaymentProcess extends StatefulWidget {
  final link;
  const PaymentProcess({Key? key, this.link}) : super(key: key);

  @override
  State<PaymentProcess> createState() => _PaymentProcessState();
}

class _PaymentProcessState extends State<PaymentProcess> {
  WebViewController? controller;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WebView(
      initialUrl: widget.link,
      onPageFinished: (string) {
        BlocProvider.of<PaymentCubit>(context).paymentListener(
            string, context, BlocProvider.of<UserCubit>(context));
      },
      javascriptMode: JavascriptMode.unrestricted,
      onWebViewCreated: (WebViewController webViewController) {},
    );
  }
}
