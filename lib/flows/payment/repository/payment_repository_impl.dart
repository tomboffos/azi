import 'dart:convert';

import 'package:azi/flows/payment/repository/payment_repository.dart';
import 'package:azi/services/network_service.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: PaymentRepository)
class PaymentRepositoryImpl implements PaymentRepository {
  final NetworkService networkService;

  PaymentRepositoryImpl(this.networkService);

  @override
  Future<String?> generateLink(String amount) async {
    final response = await networkService.authorizedRequest(
        '/payments',
        {'amount': amount, 'payment_type': 'card'},
        {'Accept': 'application/json'},
        'POST');

    return jsonDecode(response!.body)['link'];
  }
}
