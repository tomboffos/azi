import 'package:azi/flows/user/cubit/user_cubit.dart';
import 'package:azi/flows/withdraw/repository/withdraw_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

part 'withdraw_state.dart';

@injectable
class WithdrawCubit extends Cubit<WithdrawState> {
  final WithdrawRepository withdrawRepository;
  WithdrawCubit(this.withdrawRepository) : super(WithdrawInitial());

  makeWithdraw(Map<String, String> form, BuildContext context,
      UserCubit userCubit) async {
    final response = await withdrawRepository.makeWithdraw(form, context);
    userCubit.getUser();
  }
}
