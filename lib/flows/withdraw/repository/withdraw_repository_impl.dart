import 'dart:convert';

import 'package:azi/flows/withdraw/repository/withdraw_repository.dart';
import 'package:azi/services/modal_service.dart';
import 'package:azi/services/network_service.dart';
import 'package:http/src/response.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: WithdrawRepository)
class WithdrawRepositoryImpl implements WithdrawRepository {
  final ModalService modalService;
  final NetworkService networkService;

  WithdrawRepositoryImpl(this.modalService, this.networkService);

  @override
  Future<Response?> makeWithdraw(
      Map<String, String> form, BuildContext context) async {
    final response = await networkService.authorizedRequest(
        '/withdraws', form, {'Accept': 'application/json'}, 'POST');

    if (response!.statusCode == 200)
      modalService.showSuccessMessage(
          jsonDecode(response.body)['message'], context);
    else if (response.statusCode == 422) {
      String messages = '';
      jsonDecode(response.body)['errors'].forEach((k, v) {
        messages += v[0];
      });

      modalService.showErrorMessage(messages, context);
    } else
      modalService.showErrorMessage(
          jsonDecode(response.body)['message'], context);
  }
}
