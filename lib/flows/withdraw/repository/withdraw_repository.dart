import 'package:flutter/material.dart';
import 'package:http/http.dart';

abstract class WithdrawRepository {
  Future<Response?> makeWithdraw(
      Map<String, String> form, BuildContext context) async {}
}
