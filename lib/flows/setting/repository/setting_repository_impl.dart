import 'dart:convert';

import 'package:azi/data/model/setting.dart';
import 'package:azi/flows/setting/repository/interface/setting_repository.dart';
import 'package:azi/services/modal_service.dart';
import 'package:azi/services/network_service.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: SettingRepository)
class SettingRepositoryImpl implements SettingRepository {
  final NetworkService networkService;
  final ModalService modalService;

  SettingRepositoryImpl(this.networkService, this.modalService);

  @override
  Future<List<Setting>?> getSettings() async {
    final response =
        await networkService.unauthorizedRequest('/settings', 'GET');

    if (response.statusCode == 200) {
      return jsonDecode(response.body)['data']
          .map((e) => Setting.fromJson(e))
          .toList()
          .cast<Setting>();
    }
  }
}
