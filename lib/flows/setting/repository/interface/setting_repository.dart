import 'package:azi/data/model/setting.dart';

abstract class SettingRepository {
  Future<List<Setting>?> getSettings() async {}
}
