import 'package:azi/data/model/setting.dart';
import 'package:azi/flows/setting/repository/interface/setting_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

part 'setting_state.dart';

@injectable
class SettingCubit extends Cubit<SettingState> {
  final SettingRepository settingRepository;
  SettingCubit(this.settingRepository) : super(SettingInitial());

  getSettings() async {
    final settings = await settingRepository.getSettings();

    if (settings != null) {
      emit(SettingLoaded(settings));
    }
  }

  getSetting(String value) {
    if (state is SettingLoaded) {
      List values = (state as SettingLoaded)
          .settings
          .expand((e) => [if (e.key == value) e.value])
          .toList();

      emit(SettingLoaded((state as SettingLoaded).settings));
      if (values.isNotEmpty) return values.first;
    }
  }
}
