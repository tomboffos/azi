part of 'lang_cubit.dart';

@immutable
abstract class LangState {
  final Locale locale;

  LangState(this.locale);
}

class LangInitial extends LangState {
  LangInitial(Locale locale) : super(locale);
}
