import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'lang_state.dart';

@injectable
class LangCubit extends Cubit<LangState> {
  LangCubit() : super(LangInitial(const Locale('ru')));

  saveLang(Locale locale) async {
    (await SharedPreferences.getInstance())
        .setString('lang', locale.languageCode);

    emit(LangInitial(locale));
  }

  getLang() async {
    String? lang = (await SharedPreferences.getInstance()).getString('lang');

    if (lang != null) emit(LangInitial(Locale(lang)));
  }
}
