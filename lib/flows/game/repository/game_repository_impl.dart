import 'dart:convert';

import 'package:azi/common/app_colors.dart';
import 'package:azi/data/model/game.dart';
import 'package:azi/data/model/room.dart';
import 'package:azi/flows/game/repository/game_repository.dart';
import 'package:azi/services/modal_service.dart';

import 'package:azi/services/network_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:socket_io_client/socket_io_client.dart';

@Injectable(as: GameRepository)
class GameRepositoryImpl implements GameRepository {
  final NetworkService networkService;

  GameRepositoryImpl(this.networkService);

  @override
  Future<Game?> enterRoom(Room room, BuildContext context) async {
    ModalService().showMainDialog(
        context,
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              width: 100,
              height: 100,
              child: CircularProgressIndicator(
                color: AppColors.white,
              ),
            ),
          ],
        ));
    final response = await networkService.authorizedRequest(
        '/enter-room',
        {'room_id': room.id.toString()},
        {'Accept': 'application/json'},
        'POST');
    if (response!.statusCode == 400) {
      Navigator.pop(context);

      ModalService()
          .showErrorMessage(jsonDecode(response.body)['message'], context);

      return null;
    }

    return Game.fromJson(jsonDecode(response.body)['data']);
  }

  @override
  Future leaveRoom(Room room) async {
    final response = await networkService.authorizedRequest(
        '/leave/${room.id}', {}, {'Accept': 'application/json'}, 'POST');
  }

  @override
  Future<Game?> readyToGame(Game game) async {
    // TODO: implement readyToGame
    final response = await networkService.authorizedRequest(
        '/start/${game.id}', {}, {'Accept': 'application/json'}, 'POST');

    return Game.fromJson(jsonDecode(response!.body)['data']);
  }
}
