import 'package:azi/data/model/game.dart';
import 'package:azi/data/model/room.dart';
import 'package:flutter/material.dart';

abstract class GameRepository {
  Future<Game?> enterRoom(Room room, BuildContext context) async {}

  Future leaveRoom(Room room) async {}

  Future<Game?> readyToGame(
    Game game,
  ) async {}
}
