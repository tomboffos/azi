import 'dart:async';

import 'package:azi/common/app_colors.dart';
import 'package:azi/data/model/game.dart';
import 'package:azi/data/model/game_member.dart';
import 'package:azi/data/model/room.dart';
import 'package:azi/data/model/user.dart';
import 'package:azi/flows/game/repository/game_repository.dart';
import 'package:azi/services/modal_service.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:socket_io_client/socket_io_client.dart';

part 'game_state.dart';

@injectable
class GameCubit extends Cubit<GameState> {
  final GameRepository gameRepository;
  Socket socket = io(
      'https://socket.it-lead.net',
      OptionBuilder()
          .setTransports(['websocket'])
          .enableForceNewConnection()
          .build());

  GameCubit(this.gameRepository) : super(GameInitial(null));

  Future enterRoom(BuildContext context, Room room, User user) async {
    final game = await gameRepository.enterRoom(room, context);

    if (game != null) {
      addListeners(game, user, context);
      Navigator.pop(context);
      emit(EnteredToGame(game));
      Navigator.pushNamed(context, '/room', arguments: game)
          .then((value) => {leaveRoom(room)});
    }
  }

  addListeners(Game game, User user, BuildContext context) {
    socket.connect();

    socket.on('azi.player.added.${game.id}',
        (data) => emit(EnteredToGame(Game.fromJson(data))));

    socket.on('azi.player.ready.${game.id}',
        (data) => emit(EnteredToGame(Game.fromJson(data))));

    socket.on('azi.player.leave.${game.id}', (data) {
      emit(EnteredToGame(Game.fromJson(data)));
    });

    socket.on('azi.game.started.${game.room.id}', (data) {
      Future.delayed(const Duration(seconds: 10), () {
        Game newGame = Game.fromJson(data);
        emit(EnteredToGame(newGame));

        socket.disconnect();

        addListeners(newGame, user, context);
      });
    });

    socket.on('azi.user.leave.room.${user.id}', (data) {
      Future.delayed(const Duration(seconds: 5), () {
        ModalService().showErrorMessage(data['message'], context);
        Navigator.pop(context);
      });

      socket.disconnect();
    });

    socket.on('azi.game.finished.${game.id}', (data) {
      int time = 1;

      Timer.periodic(const Duration(seconds: 1), (timer) {
        ModalService().showMainDialog(
            context,
            Text(
              '$time',
              style: const TextStyle(color: Colors.white, fontSize: 40),
              textAlign: TextAlign.center,
            ));
        Future.delayed(const Duration(milliseconds: 800), () {
          Navigator.pop(context);
        });
        if (time == 3) {
          timer.cancel();
          emit(GameFinished(Game.fromJson(data)));
        }
        time++;
      });
    });
  }

  leaveRoom(Room room) async {
    socket.disconnect();

    await gameRepository.leaveRoom(room);
  }

  readyToRoom(Game game) async {
    final gameModel = await gameRepository.readyToGame(game);

    if (gameModel != null) emit(EnteredToGame(game));
  }
}
