part of 'game_cubit.dart';

@immutable
abstract class GameState {
  final Game? game;

  GameState(this.game);
}

class GameInitial extends GameState {
  GameInitial(Game? game) : super(game);
}

class EnteredToGame extends GameState {
  EnteredToGame(Game? game) : super(game);
}

class GameFinished extends GameState {
  GameFinished(Game? game) : super(game);
}
