import 'package:azi/data/model/game.dart';
import 'package:azi/data/model/user.dart';
import 'package:azi/flows/payment/screen/index.dart';
import 'package:azi/flows/payment/screen/process.dart';
import 'package:azi/flows/withdraw/cubit/withdraw_cubit.dart';
import 'package:azi/injection.dart';
import 'package:azi/presentation/auth.dart';
import 'package:azi/presentation/filterooms.dart';
import 'package:azi/presentation/game/room.dart';
import 'package:azi/presentation/home.dart';
import 'package:azi/presentation/instruction.dart';
import 'package:azi/presentation/notificationpage.dart';
import 'package:azi/presentation/registration.dart';
import 'package:azi/presentation/rooms.dart';
import 'package:azi/presentation/settings.dart';
import 'package:azi/presentation/transfer.dart';
import 'package:azi/presentation/withdraw.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AppRouter {
  Route generateRouter(RouteSettings settings) {
    final arguments = settings.arguments;
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (context) => AuthorizationPage());
      case '/register':
        return MaterialPageRoute(builder: (context) => RegistrationPage());
      case '/homepage':
        return MaterialPageRoute(builder: (context) => HomePage());
      case '/transfer':
        return MaterialPageRoute(builder: (context) => TransferPage());
      case '/instruction':
        return MaterialPageRoute(builder: (context) => InstructionIndex());
      case '/settings':
        return MaterialPageRoute(
            builder: (context) => SettingPage(
                  user: (settings.arguments as User),
                ));
      case '/notification-page':
        return MaterialPageRoute(builder: (context) => NotificationPage());

      case '/rooms':
        return MaterialPageRoute(
            builder: (context) => RoomsPage(
                  roomTypeId: arguments as int,
                ));
      case '/filter-rooms':
        return MaterialPageRoute(
            builder: (context) => FilterRooms(
                  bid: (arguments as Map<String, dynamic>)['bid'] as int,
                  roomTypeId: (arguments)['roomTypeId'] as int,
                ));
      case '/room':
        return MaterialPageRoute(
            builder: (context) => Room(
                  game: arguments as Game,
                ));
      case '/withdraw':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (context) => getIt.get<WithdrawCubit>(),
                  child: WithdrawIndex(),
                ));
      case '/payment':
        return MaterialPageRoute(builder: (context) => PaymentIndex());
      case '/payment/process':
        return MaterialPageRoute(
            builder: (context) => PaymentProcess(
                  link: arguments,
                ));
      default:
        return MaterialPageRoute(builder: (context) => HomePage());
    }
  }
}
